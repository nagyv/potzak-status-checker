<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.fontanus-it.hu
 * @since      1.0.0
 *
 * @package    Ptzk_Statuschecker
 * @subpackage Ptzk_Statuschecker/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">

    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>

    <form method="post" name="cleanup_options" action="options.php">

      <?php
          //Grab all options
          $options = get_option($this->plugin_name);

          // Cleanup
          $video_url_hu = $options['video_url_hu'];
          $video_url_en = $options['video_url_en'];
          $hirlevel_url_hu = $options['hirlevel_url_hu'];
      ?>
      <?php 
        settings_fields($this->plugin_name); 
        do_settings_sections($this->plugin_name);
      ?>
      <fieldset>
        <label for="<?php echo $this->plugin_name; ?>-video_url_hu">
          <legend><h3><?php _e('Magyar videó ID', $this->plugin_name); ?></h3></legend>
          <input type="text" class="regular-text" id="<?php echo $this->plugin_name; ?>-video_url_hu" name="<?php echo $this->plugin_name; ?>[video_url_hu]" value="<?php if(!empty($video_url_hu)) echo $video_url_hu; ?>"/>
          <label for="<?php echo $this->plugin_name; ?>-video_url_en">
          <legend><h3><?php _e('Angol videó ID', $this->plugin_name); ?></h3></legend>
          <input type="text" class="regular-text" id="<?php echo $this->plugin_name; ?>-video_url_en" name="<?php echo $this->plugin_name; ?>[video_url_en]" value="<?php if(!empty($video_url_en)) echo $video_url_en; ?>"/>
      </fieldset>

      <?php submit_button(__('Save all changes', $this->plugin_name), 'primary','submit', TRUE); ?>

    </form>

</div>
