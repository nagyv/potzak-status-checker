var player;
// onYouTubeIframeAPIReady = function() {
// 	setUpPlayer(window.YT);
// };

jQuery().ready(function() {
	loadYoutubeAPI();
	afterInit();
});

function loadYoutubeAPI () {
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	jQuery('#load-player').click(function(ev) {
		ev.preventDefault();
		setUpPlayer(window.YT);
	})
}

function setUpPlayer(YT) {
	var $player = jQuery('#player');
	player = new YT.Player('player', {
		height: '390',
		width: $player.parent().width(),
		videoId: $player.data('video'),
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
		}
	});
}

function onPlayerReady(event) {
//    event.target.playVideo();
}

function onPlayerStateChange(event) {
	if (event.data == YT.PlayerState.ENDED) {
		var $gyakorlatokPanel = jQuery('#gyakorlatok');
		var $eredmenyekPanel = jQuery('#eredmenyek');

		if(typeof ga !== 'undefined') {
			ga('set', 'page', '/allapotfelmeres/video-vege');
			ga('send', 'pageview');
		}

		$gyakorlatokPanel.collapse('hide');
		$eredmenyekPanel.collapse('show');
	}
}

function afterInit() {
	var $eredmenyekPanel = jQuery('#eredmenyek');
	var $elemzesPanel = jQuery('#elemzes')
	var $allapotfelmeresForm = jQuery('#allapotfelmeres-form');

	$allapotfelmeresForm.submit(function(ev) {
		ev.preventDefault();

		if (typeof ga !== 'undefined') {
			ga('set', 'page', '/allapotfelmeres/eredmenyek');
			ga('send', 'pageview');
		}

		var formData = new FormData(ev.target);
		var hasMinosege = 0;
		for(var pair of formData.entries()) {
				if(pair[0].startsWith("data")) {
						hasMinosege += parseInt(pair[1], 10);
				}
		}
		var panel;
		var style = 'success';

		console.log('hasMinosege', hasMinosege);

		if(hasMinosege < 0) {
				panel = '_ertekalso';
				style = 'danger';
		} else if (hasMinosege < 1 ) {
				panel = '_ertekkozepes';
				style = 'warning';
		} else {
				panel = '_ertekfelso';
		}

		console.log('panel', panel);

		$eredmenyekPanel.addClass('panel-' + style);
		$elemzesPanel.find('#_analysis_first').addClass('hidden');
		$elemzesPanel.find('#' + panel).removeClass('hidden');

		if(formData.get('data[koldokserv]')) {
				$elemzesPanel.find('#_koldokserv').removeClass('hidden');
		}

		$elemzesPanel.find('#_newsletter').removeClass('hidden');

		$eredmenyekPanel.collapse('hide');
		$elemzesPanel.collapse('show');

		var oReq = new XMLHttpRequest();
		oReq.open("POST", ev.target.action, true);
		oReq.onload = function(oEvent) {
				if (oReq.status == 200) {
				console.log('results sent')
				} else {
				console.error("Error " + oReq.status + " occurred. When sending in results");
				}
		};
		oReq.send(formData);
	})
}
