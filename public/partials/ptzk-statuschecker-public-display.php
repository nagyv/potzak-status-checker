<?php

/**
* Provide a public-facing view for the plugin
*
* This file is used to markup the public-facing aspects of the plugin.
*
* @link       https://www.fontanus-it.hu
* @since      1.0.0
*
* @package    Ptzk_Statuschecker
* @subpackage Ptzk_Statuschecker/public/partials
*/
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="accordian fusion-accordian panel-group" id="allapotfelmeres" role="tablist" aria-multiselectable="true">
  <div class="fusion-panel panel panel-default">
    <div class="panel-heading" role="tab" id="gyakorlatokCim">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#allapotfelmeres" href="#gyakorlatok" aria-expanded="true" aria-controls="gyakorlatok"><?php _e('status-checking-title', $this->plugin_name); ?></a>
      </h4>
    </div>
    <div id="gyakorlatok" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="gyakorlatokCim">
      <div class="panel-body">
        <div id="player" data-video="<?php echo $this->options['video_url_' . pll_current_language()]; ?>">
          <p><?php _e('no-player', $this->plugin_name); ?></p>
          <button id="load-player" class="fusion-button button-flat button-round button-large button-default button-1" type="button">
            <span class="fusion-button-text"><?php _e('player-load', $this->plugin_name); ?></span>
          </button>
        </div>
      </div>
    </div>
  </div>
  <div class="fusion-panel panel panel-default">
    <div class="panel-heading" role="tab" id="eredmenyekCim">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#allapotfelmeres" href="#eredmenyek" aria-expanded="false" aria-controls="eredmenyek"><?php _e('status-checking-results', $this->plugin_name); ?></a>
      </h4>
    </div>
    <div id="eredmenyek" class="panel-collapse collapse" role="tabpanel" aria-labelledby="eredmenyekCim">
      <div class="panel-body text-left">
        <form name="allapotfelmeresForm" action="/allapotfelmeres" method="POST" id="allapotfelmeres-form">
          <fieldset>
            <legend><?php _e('q1: umbilical hernia', $this->plugin_name); ?></legend>
              <div class="form-group">
              <input name="data[koldokserv]" value="-6" type="checkbox" id="koldokserv"/>
              <div class="btn-group">
                <label class="btn btn-primary" for="koldokserv">
                  <span class="fa fa-check"></span>
                  <span></span>
                </label>
                <label class="btn btn-default" for="koldokserv"><?php _e('koldokserv-question', $this->plugin_name); ?></label>
              </div>
            </div>
          </fieldset>              
          <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
              <div class="form-display-wrapper " >
                <h5><?php _e('bellychecks-title', $this->plugin_name); ?></h5>
              </div>
            </div>
          </div>
          <fieldset>
            <legend><?php _e('belly muscles', $this->plugin_name); ?></legend>
            <div class="form-group">
              <input name="data[hasizom1]" value="1" type="checkbox" id="hasizom1"/>
              <div class="btn-group">
                <label class="btn btn-primary" for="hasizom1">
                  <span class="fa fa-check"></span>
                  <span></span>
                </label>
                <label class="btn btn-default" for="hasizom1"><?php _e('bellycheck-q1', $this->plugin_name); ?></label>
              </div>
            </div>

            <div class="form-group">
              <input name="data[hasizom2]" value="1" type="checkbox" id="hasizom2"/>
              <div class="btn-group">
              <label class="btn btn-primary" for="hasizom2">
                <span class="fa fa-check"></span>
                <span></span>
              </label>
              <label class="btn btn-default" for="hasizom2"><?php _e('bellycheck-q2', $this->plugin_name); ?></label>
            </div>
            </div>
          </fieldset>

          <fieldset>
            <legend><?php _e('rectus-diastasis', $this->plugin_name); ?></legend>
            <div class="form-group">
              <input name="data[nyilt-has1]" value="-2" type="checkbox" id="nyilt-has1"/>
              <div class="btn-group">
                <label class="btn btn-primary" for="nyilt-has1">
                  <span class="fa fa-check"></span>
                  <span></span>
                </label>
                <label class="btn btn-default" for="nyilt-has1"><?php _e('redi-q1', $this->plugin_name); ?></label>
              </div>
            </div>

            <div class="form-group">
              <input name="data[nyilt-has2]" value="-2" type="checkbox" id="nyilt-has2"/>
              <div class="btn-group">
                <label class="btn btn-primary" for="nyilt-has2">
                  <span class="fa fa-check"></span>
                  <span></span>
                </label>
                <label class="btn btn-default" for="nyilt-has2"><?php _e('redi-q2', $this->plugin_name); ?></label>
              </div>
            </div>
          </fieldset> 
          <div class="row">
            <div class="form-group col-xs-12">
              <button class="fusion-button button-flat button-round button-large button-default button-1" type="submit">
                <span class="fusion-button-text"><?php _e('what-to-do-button', $this->plugin_name); ?></span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="fusion-panel panel panel-default">
    <div class="panel-heading" role="tab" id="elemzesCim">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#allapotfelmeres" href="#elemzes" aria-expanded="false" aria-controls="eredmenyek"><?php _e('analysis-title', $this->plugin_name); ?></a>
      </h4>
    </div>
    <div id="elemzes" class="panel-collapse collapse" role="tabpanel" aria-labelledby="elemzesCim">
      <div class="panel-body">
        <div id="_analysis_first"><?php _e('analysis-text', $this->plugin_name);?></div>
        <div id="_koldokserv" class="hidden"><?php _e('koldokserv', $this->plugin_name);?></div>
        <div id="_ertekalso" class="hidden"><?php _e('ertekalso', $this->plugin_name);?></div>
        <div id="_ertekkozepes" class="hidden"><?php _e('ertekkozepes', $this->plugin_name);?></div>
        <div id="_ertekfelso" class="hidden"><?php _e('ertekfelso', $this->plugin_name);?></div>
        <div id="_newsletter" class="hidden"><?php 
          if (pll_current_language() == 'hu') {
            $mailchimp_shortode = '[mc4wp_form id="108"]';
          } else {
            $mailchimp_shortode = '[mc4wp_form id="111"]';
          }
          echo do_shortcode($mailchimp_shortode);
        ?>
        </div>
      </div>
    </div>
  </div>
</div>