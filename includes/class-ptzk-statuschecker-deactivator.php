<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.fontanus-it.hu
 * @since      1.0.0
 *
 * @package    Ptzk_Statuschecker
 * @subpackage Ptzk_Statuschecker/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ptzk_Statuschecker
 * @subpackage Ptzk_Statuschecker/includes
 * @author     Viktor Nagy <viktor.nagy@gmail.com>
 */
class Ptzk_Statuschecker_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
