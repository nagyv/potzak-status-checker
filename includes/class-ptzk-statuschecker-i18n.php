<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.fontanus-it.hu
 * @since      1.0.0
 *
 * @package    Ptzk_Statuschecker
 * @subpackage Ptzk_Statuschecker/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ptzk_Statuschecker
 * @subpackage Ptzk_Statuschecker/includes
 * @author     Viktor Nagy <viktor.nagy@gmail.com>
 */
class Ptzk_Statuschecker_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ptzk-statuschecker',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
