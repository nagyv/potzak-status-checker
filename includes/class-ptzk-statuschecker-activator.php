<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.fontanus-it.hu
 * @since      1.0.0
 *
 * @package    Ptzk_Statuschecker
 * @subpackage Ptzk_Statuschecker/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ptzk_Statuschecker
 * @subpackage Ptzk_Statuschecker/includes
 * @author     Viktor Nagy <viktor.nagy@gmail.com>
 */
class Ptzk_Statuschecker_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
