<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.fontanus-it.hu
 * @since             1.0.0
 * @package           Ptzk_Statuschecker
 *
 * @wordpress-plugin
 * Plugin Name:       Potzak Statuschecker
 * Plugin URI:        https://bitbucket.org/nagyv/potzak-status-checker
 * Description:       A Potzak állapotfelmérő videója és visszajelzése
 * Version:           1.0.17
 * Author:            Viktor Nagy
 * Author URI:        https://www.fontanus-it.hu
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ptzk-statuschecker
 * Domain Path:       /languages
 * Bitbucket Plugin URI nagyv/potzak-status-checker
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'PLUGIN_NAME_VERSION', '1.0.17' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ptzk-statuschecker-activator.php
 */
function activate_ptzk_statuschecker() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ptzk-statuschecker-activator.php';
	Ptzk_Statuschecker_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ptzk-statuschecker-deactivator.php
 */
function deactivate_ptzk_statuschecker() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ptzk-statuschecker-deactivator.php';
	Ptzk_Statuschecker_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ptzk_statuschecker' );
register_deactivation_hook( __FILE__, 'deactivate_ptzk_statuschecker' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ptzk-statuschecker.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ptzk_statuschecker() {

	$plugin = new Ptzk_Statuschecker();
	$plugin->run();

}
run_ptzk_statuschecker();
