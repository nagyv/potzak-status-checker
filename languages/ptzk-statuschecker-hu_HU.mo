��          �      l      �     �     �     �               (  	   :  
   D     O  
   \     g  	   {     �     �     �     �     �     �     �     �  �    C   �            B     9   `  C   �  =  �  >    �  [  �  6
     �  7   �          "     0  E   >  B   �     �     �     �        	                                              
                                            analysis-text analysis-title belly muscles bellycheck-q1 bellycheck-q2 bellychecks-title ertekalso ertekfelso ertekkozepes koldokserv koldokserv-question no-player player-load q1: umbilical hernia rectus-diastasis redi-q1 redi-q2 status-checking-results status-checking-title what-to-do-button Project-Id-Version: Potzak Statuschecker
POT-Creation-Date: 2017-11-14 19:42+0100
PO-Revision-Date: 2017-11-15 11:05+0100
Last-Translator: Viktor Nagy <viktor.nagy@gmail.com>
Language-Team: Potzak
Language: hu_HU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.4
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Az elemzés megtekintése előtt végezd el az állapotfelmérést. Elemzés Hasizom A fejem megemelése nélkül is éreztem a bőr alatt a hasizmokat A fejem megemelésekor éreztem a bőr alatt a hasizmokat A kezed végighúzásakor a hasfalon tapasztaltad-e az alábbiakat? <h6>Csak így tovább!</h6>
<p>Az eredmények alapján alapvetően erős a hasfalad. Ha az állapotfelmérés során kitüremkedett a hasad közepe, akkor nagy valószínűséggel szétnyíltak a hasizmaid, ezért mindenképpen ajánljuk a Potzak-ot, hogy hosszú távon megfelelő módon rehabilitáld a hasadat.</p> <h6>Mindenképpen dolgozz a hasfaladon!</h6>
<p>Az eredmények alapján valószínűleg szétnyíltak a hasizmaid, ezért mindenképpen javasolt számodra a Potzak gyakorlatainak elkezdése. Az alkalmazás segítségével, ha kitartó vagy a gyakorlásban, ezen az állapoton néhány hónap alatt változtathatsz.</p> <h6>Dolgozz a hasadon!</h6>
<p>A teszteredmények alapján úgy tűnik, alapvetően gyenge a hasfalad, jót tenne neki, ha dolgoznál rajta.. A Potzak használatával néhány hét alatt jelentős javulást érhetsz el a hasad állapotában, pár hónap alatt pedig - ha rendszeresen végzed a kiadott gyakorlatokat - várhatóan el fognak múlni a tüneteid. Rendszeres edzéssel, és a megfelelő feladatok végzésével, telesen egészséges és tünetmentes lehetsz.</p> <h6>A köldöksérvre figyelj oda</h6>
<p>A köldöksérv minden esetben odafigyelést igényel, de nem minden esetben szükséges műtéti megoldás. Ha köldöksérved van, végezheted a Potzak gyakorlatait, de figyeld a hasadat: ha a köldöksérvednél szúró fájdalmat érzékelsz (akár nap közben, a gyakorlatok végzésétől függetlenül is), fordulj a háziorvosodhoz.</p> Köldöksérvem van Amennyiben a videó nem jelenik meg, kattints a gombra. Videólejátszó betöltése Köldöksérv Nyílt hasfal A köldök fölött, a két hasizom közt mélyebbre mentek az ujjaim A köldök alatt, a két hasizom közt mélyebbre mentek az ujjaim Eredmények Állapotfelmérés Mit tegyek vele? 